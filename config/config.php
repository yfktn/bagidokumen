<?php

/**
 * integrasi dengan plugin offline.sitesearch untuk pencarian universal
 * 
 */

return [
    // merupakan setting untuk integrasi dengan plugin offline.sitesearch
    'offlineSiteSearchResult' => [
        // set nilai ini dengan url untuk detail sebuah record tulisan
        'url'         => 'dokumen/detail',
        // ini dengan nilai param detailnya, pilihan yang ada adalah: 
        // id atau slug
        'paramDetail' => 'slug',
        // kategori ini nantinya ditampilkan di halaman hasil pencarian!
        'provider' => 'Dokumen'
    ],
    'jenisInformasi' => [
        "tersedia-setiap-saat" => 'Tersedia Setiap Saat',
        "berkala" => 'Berkala',
        "serta-merta" => 'Serta Merta',
        "dikecualikan" => 'Dikecualikan',
    ]
];

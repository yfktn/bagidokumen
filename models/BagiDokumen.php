<?php namespace Panatau\BagiDokumen\Models;

use Model;

/**
 * Model
 */
class BagiDokumen extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;
    
    /**
     * @var array untuk setting slug digenerate dari nama dan id
     */
    protected $slugs = [ 'slug' => 'nama' ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'panatau_bagidokumen_';

    protected $jsonable = ['clouds'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'nama' => 'required',
        // 'filenya' => 'required'
    ];

    public $belongsToMany = [
        'catnya' => [
            'Panatau\BagiDokumen\Models\BagiDokumenCat', 
            'table' => 'panatau_bagidokumen_catnya',
            'key' => 'dokumen_id',
            'otherKey' => 'cat_id'
        ]
    ];

    public $attachMany = [
        'filenya' => ['System\Models\File','order' => 'sort_order']
    ];

    public function scopeBerdasarkanTagBackendList($query, $tagId) 
    {
        if(!is_array($tagId)) {
            $tagId = [$tagId];
        }
        return $query->whereHas('catnya', function($query) use($tagId) {
            $query->whereIn('cat_id', $tagId);
        });  
    }

    /**
     * Lakukan filter berdasarkan tag. Bila tag bernilai all, maka jangan lakukan filter!
     *
     * @param [type] $query
     * @param [type] $tag
     * @return void
     */
    public function scopeBerdasarkanTag($query, $tag) {
        if(strlen($tag) == 0 || $tag == 'default' || $tag == 'all') {
            return $query;
        }
        return $query->whereHas('catnya', function($query) use($tag) {
            $query->where('cat_id', $tag);
        });
    }

    /**
     * Kalau yang satu berdasarkan id, sekarang ini berdasarkan slug di tagnya!
     * @param mixed $query 
     * @param mixed $tag 
     * @return mixed 
     */
    public function scopeBerdasarkanTagSlug($query, $tag)
    {
        if (strlen($tag) == 0 || $tag == 'default' || $tag == 'all') {
            return $query;
        }
        return $query->whereHas('catnya', function ($query) use ($tag) {
            $query->where('slug', $tag);
        });
    }

    public function scopeBerdasarkanJenisInformasi($query, $tag)
    {
        if(empty($tag)) {
            return $query;
        }

        return $query->where('jenis_informasi', $tag);
    }

    /**
     * Ini ditarik dari template yang ingin melakukan generate terhadap link supaya ditampilkan datanya
     * per klasifikasi. Hal ini dilakukan karena kita tidak bisa mendapatkan satu-satu nilainya langsung
     * untuk tag.id
     *
     * @param [type] $pageName nama page yang memanggil
     * @param [type] $controller object controller daripada component
     * @param [type] $paramPage ini digenerate oleh object component
     * @param [type] $paramTagName nama parameter untuk nilai tag id
     * @param BagiDokumenCat $paramTagId ini adalha record dari tag nya
     * @return url yang telah digenerate
     */
    public function generateTagUrl($pageName, $controller, $paramPage, $paramTagName, BagiDokumenCat $tagRecord) {
        $recordRef = $tagRecord->id; // id kalau selain :slug
        if($paramTagName == 'slug' || $paramTagName == 'tagslug') {
            // bila url variable menggunakan :slug maka set referensi dari nilai slug nya!
            $recordRef = $tagRecord->slug;
        }
        $paramPage[$paramTagName] = $recordRef;
        return $controller->pageUrl($pageName, $paramPage);
    }

    public function getUrlAttribute()
    {
        return isset($this->attributes['url']) ? $this->attributes['url']: '';
    }

    public function setUrl($pageName, $controller, $urlParams = [])
    {
        $params = [
            array_get($urlParams, 'id', 'id') => $this->id,
            array_get($urlParams, 'slug', 'slug') => $this->slug,            
        ];
        return $this->attributes['url'] = $controller->pageUrl($pageName, $params);
    }

    /**
     * Render jenis informasi yang bisa ditambahkan!
     * @return string[] 
     */
    public static function getJenisInformasi()
    {
        return config("panatau.bagidokumen::jenisInformasi");
    }

    /**
     * Ini untuk menampilkan pilihan pada isian jenis informasi
     * @return string[] 
     */
    public function getJenisInformasiOptions()
    {
        return self::getJenisInformasi();
    }

}

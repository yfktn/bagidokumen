<?php namespace Panatau\BagiDokumen\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;

/**
 * Model
 */
class BagiDokumenCat extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Sluggable;

    protected $slugs = [
        'slug' => 'nama'
    ];
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'panatau_bagidokumen_cat';
    
    protected $fillable = ['nama'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'nama' => 'required|min:4'
    ];

    public function getSelectedTagNameByTagId($val) {
        return optional(\Panatau\BagiDokumen\Models\BagiDokumenCat::find($val))->nama;
    }

    public function getTagOptions() {
        return BagiDokumenCat::lists('nama', 'id');
    }

    public function getUrl($test) {
        return $test;
    }
}

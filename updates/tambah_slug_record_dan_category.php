<?php namespace Panatau\BagiDokumen\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class TambahSlugRecordDanCategory extends Migration 
{
    public function up()
    {
        Schema::table('panatau_bagidokumen_', function($table)
        {
            $table->string('slug')->nullable()->index();
        });
        
        Schema::table('panatau_bagidokumen_cat', function($table)
        {
            $table->string('slug')->nullable()->index();
        });
    }
    
    public function down()
    {
        // nothing to do ... just leave it!
    }
}
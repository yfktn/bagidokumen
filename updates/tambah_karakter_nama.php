<?php namespace Panatau\BagiDokumen\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class TambahKarakterNama extends Migration 
{
    public function up()
    {
        Schema::table('panatau_bagidokumen_', function($table)
        {
            $table->string('nama', 1024)->change();
        });
    }
    
    public function down()
    {
        // nothing to do ... just leave it!
    }
}
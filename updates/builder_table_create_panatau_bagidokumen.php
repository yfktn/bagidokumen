<?php namespace Panatau\BagiDokumen\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePanatauBagidokumen extends Migration
{
    public function up()
    {
        Schema::create('panatau_bagidokumen_cat', function($table) 
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('nama');
        });
        Schema::create('panatau_bagidokumen_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('nama');
            $table->text('keterangan')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('izin')->nullable()->default('public')->index();
            $table->string('tags')->nullable();
        });
        Schema::create('panatau_bagidokumen_catnya', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('dokumen_id')->unsigned()->index();
            $table->integer('cat_id')->unsigned()->index();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('panatau_bagidokumen_catnya');
        Schema::dropIfExists('panatau_bagidokumen_');
        Schema::dropIfExists('panatau_bagidokumen_cat');
    }
}
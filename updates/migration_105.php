<?php namespace Panatau\BagiDokumen\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration105 extends Migration
{
    public function up()
    {
        Schema::table('panatau_bagidokumen_', function($table)
        {
            $table->string('jenis_informasi', 30)->nullable()->index();
        });
    }

    public function down()
    {
        Schema::table('panatau_bagidokumen_', function($table)
        {
            $table->dropColumn('jenis_informasi');
        });
    }
}
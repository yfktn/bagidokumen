Plugin ini digunakan untuk memberikan fasilitas membagi dokumen, serta fasilitas tag untuk memberikan klasifikasi terhadap dokumen yang dibagikan.

Satu buah dokumen akan terdiri atas:

- nama dokumen
- keterangan
- tags (bisa lebih dari satu tag)
- file yang di-share (bisa lebih dari satu file)

### instalasi

Masuk ke folder plugins octobercms lalu:

```
$ mkdir panatau
$ cd panatau
$ git clone https://gitlab.com/yfktn/bagidokumen.git .
```

### utility untuk generate slug

Bila didapatkan ada slug yang tidak digenerate, maka `super admin` dapat menggunakan utility untuk melakukan generate terhadap slug yang belum tergenerate. Mengakses url:

```
https://domain.anda/backend/panatau/bagidokumen/bagidokumen/generateSlug
```

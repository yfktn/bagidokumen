<?php namespace Panatau\BagiDokumen;

use System\Classes\PluginBase;
use Panatau\BagiDokumen\Models\BagiDokumen;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Panatau\BagiDokumen\Components\DaftarDokumen' => 'daftarDokumen',
            'Panatau\BagiDokumen\Components\DaftarBerdasarkanJenisInformasi' => 'daftarBerdasarkanJenisInformasi',
            'Panatau\BagiDokumen\Components\DaftarTags' => 'daftarTags',
            'Panatau\BagiDokumen\Components\DetailDokumen' => 'detailDokumen'
        ];
    }

    public function registerSettings()
    {
    }

    public function boot() 
    {
        // lakukan penambahan untuk mendengarkan pencarian dokumen
        \Event::listen('offline.sitesearch.query', function($query) {
            // lakukan query
            $items = BagiDokumen::where('nama', 'like', "%{$query}%")
                ->orWhere('keterangan', 'like', "%{$query}%")
                ->get();
            // bangun hasilnya
            $results = $items->map(function($item) use($query) {
                // If the query is found in the title, set a relevance of 2
                $relevance = mb_stripos($item->nama, $query) !== false ? 2: 1;
                $generatedUrl = \Config::get('panatau.bagidokumen::offlineSiteSearchResult.url');
                if (\Config::get('panatau.bagidokumen::offlineSiteSearchResult.paramDetail') == 'slug') {
                    $generatedUrl .= '/' . $item->slug;
                } else {
                    $generatedUrl .= '/' . $item->id;
                }
                return [
                    'title' => $item->nama,
                    'text'  => $item->keterangan,
                    'url'   => $generatedUrl,
                    // 'thumb'     => $item->images->first(), // Instance of System\Models\File
                    'relevance' => $relevance, // higher relevance results in a higher
                                               // position in the results listing
                    // 'meta' => 'data',       // optional, any other information you want
                                               // to associate with this result
                    // 'model' => $item,       // optional, pass along the original model
                ];
            });
            return [
                'provider' => 'Dokumen',
                'results'  => $results
            ];
        });
    }
}

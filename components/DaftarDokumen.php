<?php namespace Panatau\BagiDokumen\Components;
use Panatau\BagiDokumen\Models\BagiDokumen as BagiDokumenModel;
use Panatau\BagiDokumen\Models\BagiDokumenCat;

class DaftarDokumen extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'Daftar Dokumen Dibagikan',
            'description' => 'Menampilkan daftar dokumen dibagikan'
        ];
    }

    public function defineProperties() {
        return [
            'jumlahItem' => [
                'title' => 'Item Tampilan',
                'description' => 'Jumlah item ditampilkan',
                'default' => 40
            ],
            'paramTag' => [
                'title' => 'Parameter Tag',
                'description' => 'Parameter id tag dokumen terpilih, ":tagslug" untuk slug',
                'type' => 'string',
                'default' => '{{ :tagid }}'
            ],
            'paramHalaman' => [
                'title' => 'Parameter Halaman',
                'description' => 'Parameter menunjukkan halaman aktif yang di load',
                'type' => 'string',
                'default' => '{{ :page }}'
            ],
            'halamanDetail' => [
                'title' => 'Halaman Detail',
                'description' => 'Pilih halaman detail',
                'type' => 'dropdown',
                'default' => 'dokumen/detail'
            ],
        ];
    }

    public function getHalamanDetailOptions()
    {
        return \Cms\Classes\Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Dapatkan nama categori terpilih!
     * @param mixed $tagSlug 
     * @return mixed 
     */
    protected function getCatName($tagSlug)
    {
        if(isset($tagSlug[0]) && $tagSlug != 'default') {
            return optional(BagiDokumenCat::where('slug', $tagSlug)->first())->nama;
        }
        return null;
    }

    protected function siapkanVariable() {
        $this->page['paramHalaman'] = $this->paramName('paramHalaman');
        $this->page['halamanAktif'] = (int) $this->property('paramHalaman', 1);
        $this->page['halamanDetail'] = (int) $this->property('halamanDetail');
        $this->page['paramTag'] = $this->paramName('paramTag');
        $this->page['tagTerpilih'] = $this->property('paramTag', '');
        $this->page['tagTerpilihNama'] = $this->getCatName($this->page['tagTerpilih']);
        $this->page['jumlahItemPerHalaman'] = (int) $this->property('jumlahItem');
        $this->page['paramPage'] = [
            $this->page['paramTag'] => $this->page['tagTerpilih'],
            $this->page['paramHalaman'] => $this->page['halamanAktif'] == 0? 1: $this->page['halamanAktif']
        ];
        $this->page['resetUrl'] = $this->controller->pageUrl($this->page->id, [
            $this->page['paramTag'] => "",
            $this->page['paramHalaman'] => 1
        ]);
    }

    public function dapatkanData() {
        $posts = new BagiDokumenModel();
        if(strlen($this->page['tagTerpilih'])>0) {
            if($this->page['paramTag'] == 'slug' || $this->page['paramTag'] == 'tagslug') {
                $posts = $posts->berdasarkanTagSlug($this->page['tagTerpilih']);
            } else {
                $posts = $posts->berdasarkanTag($this->page['tagTerpilih']);
            }
        }
        $posts = $posts
            ->orderBy('updated_at', 'desc')
            ->paginate($this->page['jumlahItemPerHalaman'], $this->page['halamanAktif']);
        $posts->each(function($post) {
            $post->setUrl($this->page['halamanDetail'], $this->controller, [
                'id' => 'id', 'slug' => 'slug'
            ]);
        });
        return $posts;
    }

    public function onRun() {
        $this->siapkanVariable();
        $this->page['posts'] = $this->dapatkanData();
    }
}
<?php namespace Panatau\BagiDokumen\Components;
use Panatau\BagiDokumen\Models\BagiDokumen as BagiDokumenModel;
use Panatau\BagiDokumen\Models\BagiDokumenCat;

class DaftarBerdasarkanJenisInformasi extends \Cms\Classes\ComponentBase 
{
    public $dataTerpilih = [];

    public function componentDetails() {
        return [
            'name'        => 'Daftar Dokumen Berdasarkan Jenis Informasi',
            'description' => 'Menampilkan daftar dokumen berdasarkan jenis informasi.'
        ];
    }

    public function defineProperties() {
        return [
            'jumlahItem' => [
                'title' => 'Item Tampilan',
                'description' => 'Jumlah item ditampilkan',
                'default' => 40
            ],
            'paramJenisInformasi' => [
                'title' => 'Parameter Jenis Informasi',
                'description' => 'Parameter jenis informasi.',
                'type' => 'string',
                'default' => '{{ :jenisInformasiSlug }}'
            ],
            'paramHalaman' => [
                'title' => 'Parameter Halaman',
                'description' => 'Parameter menunjukkan halaman aktif yang di load',
                'type' => 'string',
                'default' => '{{ :page }}'
            ],
            'halamanDetail' => [
                'title' => 'Halaman Detail',
                'description' => 'Pilih halaman detail',
                'type' => 'dropdown',
                'default' => 'dokumen/detail'
            ],
        ];
    }

    public function getHalamanDetailOptions()
    {
        return \Cms\Classes\Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    protected function siapkanVariable() {
        $this->dataTerpilih['paramHalaman'] = $this->paramName('paramHalaman');
        $this->dataTerpilih['halamanAktif'] = (int) $this->property('paramHalaman', 1);
        $this->dataTerpilih['halamanDetail'] = (int) $this->property('halamanDetail');
        $this->dataTerpilih['jenisInformasi'] = $this->property('paramJenisInformasi');
        $this->dataTerpilih['paramJenisInformasi'] = $this->paramName('paramJenisInformasi');
        $this->dataTerpilih['jumlahItemPerHalaman'] = (int) $this->property('jumlahItem');
        $this->dataTerpilih['paramPage'] = [
            $this->dataTerpilih['paramJenisInformasi'] => $this->dataTerpilih['jenisInformasi'],
            $this->dataTerpilih['paramHalaman'] => $this->dataTerpilih['halamanAktif'] == 0? 1: $this->dataTerpilih['halamanAktif']
        ];
        $this->dataTerpilih['resetUrl'] = $this->controller->pageUrl($this->page->id, [
            $this->dataTerpilih['paramJenisInformasi'] => "",
            $this->dataTerpilih['paramHalaman'] => 1
        ]);
    }

    public function dapatkanData() {
        $posts = new BagiDokumenModel();
        $posts = $posts
            ->where('jenis_informasi', $this->dataTerpilih['jenisInformasi'])
            ->orderBy('updated_at', 'desc')
            ->paginate($this->dataTerpilih['jumlahItemPerHalaman'], $this->dataTerpilih['halamanAktif']);
        $posts->each(function($post) {
            $post->setUrl($this->dataTerpilih['halamanDetail'], $this->controller, [
                'id' => 'id', 'slug' => 'slug'
            ]);
        });
        return $posts;
    }

    public function onRun() {
        $this->siapkanVariable();
        $this->dataTerpilih['posts'] = $this->dapatkanData();
    }
}
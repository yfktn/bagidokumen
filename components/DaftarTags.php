<?php namespace Panatau\BagiDokumen\Components;

use Db;
use Cms\Classes\ComponentBase;

class DaftarTags extends ComponentBase
{

    public $posts = null;

    public function componentDetails() 
    {
        return [
            'name'        => 'Daftar Tags',
            'description' => 'Menampilkan daftar tags & jumlah'
        ];
    }

    public function defineProperties()
    {
        return [
            'halamanDaftarDokumen' => [
                'title' => 'Halaman Daftar Dokumen',
                'description' => 'Pilih halaman daftar dokumen atas tag terpilih',
                'type' => 'dropdown',
                'default' => 'dokumen/daftar'
            ],
        ];
    }

    public function getHalamanDaftarDokumenOptions()
    {
        return \Cms\Classes\Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function siapkanVariable()
    {
        $this->page['halamanDaftarDokumen'] = $this->property('halamanDaftarDokumen');
    }

    public function loadPosts()
    {
        // lakukan loading tags dan dapatkan jumlah dokumen didalamnya ...
        $posts = Db::table('panatau_bagidokumen_cat')
            ->join('panatau_bagidokumen_catnya', 'panatau_bagidokumen_cat.id', '=', 'panatau_bagidokumen_catnya.cat_id')
            ->join('panatau_bagidokumen_', 'panatau_bagidokumen_.id', '=', 'panatau_bagidokumen_catnya.dokumen_id')
            ->select(Db::raw('panatau_bagidokumen_cat.nama, panatau_bagidokumen_cat.slug, panatau_bagidokumen_cat.id, count(*) as jumlah, null as url'))
            ->groupBy('panatau_bagidokumen_cat.nama', 'panatau_bagidokumen_cat.slug', 'panatau_bagidokumen_cat.id')
            ->get();
        $posts->each(function($item) {
            $item->url = $this->controller->pageUrl($this->page['halamanDaftarDokumen'], [
                'tagid' => $item->id,
                'tagslug' => $item->slug,
                'slug' => $item->slug
            ]);
        });
        return $posts;
    }

    public function onRun()
    {
        $this->siapkanVariable();
        $this->posts = $this->loadPosts();
    }
}
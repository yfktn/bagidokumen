<?php namespace Panatau\BagiDokumen\Components;
use Panatau\BagiDokumen\Models\BagiDokumen as BagiDokumenModel;
use Cms\Classes\Page;


class DetailDokumen extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'Detail Dokumen Dibagikan',
            'description' => 'Menampilkan detail dokumen dibagikan'
        ];
    }

    public function defineProperties() {
        return [
            'paramId' => [
                'title' => 'Parameter Id',
                'description' => 'Parameter id dokumen, gunakan ":id" untuk id dan ":slug" utk menggunakan slug',
                'type' => 'string',
                'default' => '{{ :id }}'
            ],
            'halamanDaftar' => [
                'title' => 'Halaman daftar',
                'description' => "Pilih halaman daftar dokumen",
                'type' => 'dropdown',
                'default' => 'dokumen',
            ]
        ];
    }

    public function getHalamanDaftarOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }


    protected function siapkanVariable() {
        $this->page['paramId'] = $this->paramName('paramId');
        // check apakah menggunakan id atau slug?
        if($this->page['paramId'] == 'id') {
            $this->page['paramIdValue'] = (int) $this->property('paramId', 1);
        } else {
            $this->page['paramIdValue'] = $this->property('paramId', '');
        }
        $this->page['halamanDaftar'] = $this->property('halamanDaftar');
    }

    /**
     * Dapatkan data yang terpilih melalui id yang diberikan.
     * @return BagiDokumenModel model yang didapat, NULL bila tidak dapat 
     */
    public function dapatkanData()
    {
        if ($this->page['paramId'] == 'id') {
            return BagiDokumenModel::find($this->page['paramIdValue']);
        }
        return BagiDokumenModel::where('slug', $this->page['paramIdValue'])->first();
    }

    public function onRun() {
        $this->siapkanVariable();
        $this->page['post'] = $this->dapatkanData();
    }
}
<?php namespace Panatau\BagiDokumen\Controllers;

use App;
use Backend\Classes\Controller;
use BackendAuth;
use BackendMenu;
use Panatau\BagiDokumen\Models\BagiDokumenCat;
use Panatau\BagiDokumen\Models\BagiDokumen as BagiDokumenModel;

class BagiDokumen extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'panatau.bagidokumen.atur' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Panatau.BagiDokumen', 'main-menu-dok');
    }

    /**
     * Ini dipakai untuk melakukan generate data di BagiDokumen yang belum ada slug nya, serta pula melakukan
     * generate slug pada bagian Tag nya!
     * @return void 
     */
    public function generateSlug()
    {
        $this->pageTitle = "Slug generator!";
        // hanya si super user yang bisa melakukan generate ini!
        if(!BackendAuth::check() && !BackendAuth::getUser()->isSuperUser()) {
            App::abort(403, 'Hanya Super User!');
        }
        $dokumen = BagiDokumenModel::where('slug', null)->get();
        foreach($dokumen as $d) {
            $d->slugAttributes();
            $d->save();
        }
        // update untuk semua tag
        $tags = BagiDokumenCat::where('slug', null)->get();
        foreach($tags as $tag) {
            $tag->slugAttributes();
            $tag->save();
        }
    }
}
